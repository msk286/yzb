angular.module('starter.controllers', [])

    .controller('AppCtrl', function ($scope, $ionicModal, $timeout,$state) {

        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //$scope.$on('$ionicView.enter', function(e) {
        //});

        $scope.valueS = false;
        $scope.typeValue = 'password';


        $scope.changeValue = function () {
            $scope.valueS = !$scope.valueS;
            $scope.typeValue = $scope.typeValue == 'password' ? 'text' : 'password';
            $timeout(function () {
                console.log('work');
                if($scope.valueS){
                    $scope.valueS = !$scope.valueS;
                    $scope.typeValue = $scope.typeValue == 'password' ? 'text' : 'password';
                }
            },3000);
        };



        $scope.imgData = {};
        document.addEventListener("deviceready", function () {
            $scope.scanBarcode = function () {
                $cordovaBarcodeScanner.scan().then(function (imageData) {
                    console.log(imageData);
                    $scope.imgData = imageData;
                    console.log(imageData.text);
                    console.log("Barcode Format -> " + imageData.format);
                    console.log("Cancelled -> " + imageData.cancelled);
                }, function (error) {
                    console.log("An error happened -> " + error);
                });
            };
        }, false);


        $scope.slideNo = [1, 2, 3, 4, 5, 6];
        $scope.goToView = function (viewName) {
            $state.go("app." + viewName)
        };
        // Form data for the login modal
        $scope.loginData = {};

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/login.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modal = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeLogin = function () {
            $scope.modal.hide();
        };

        // Open the login modal
        $scope.login = function () {
            $scope.modal.show();
        };

        // Perform the login action when the user submits the login form
        $scope.doLogin = function () {
            console.log('Doing login', $scope.loginData);

            // Simulate a login delay. Remove this and replace with your login
            // code if using a login system
            $timeout(function () {
                $scope.closeLogin();
            }, 1000);
        };
    })

    .controller('homeCtrl', function ($scope, $ionicActionSheet, $state) {
        $scope.slideChanged = function (index) {
            console.log(index);
        };
        $scope.activeSlide = function (index) {
            console.log(index);
        };
        $scope.numberImg = [11, 22, 44, 33, 55, 66];
        $scope.tasks = [{img: "A1", name: "Programs"}, {img: "A2", name: "Şu An"},
            {img: "A3", name: "Konuşmacılar"}, {img: "A4", name: "Ajandam"},
            {img: "A5", name: "Twitter"}, {img: "A6", name: "Instagram"},
            {img: "A7", name: "Fotoğraf"}, {img: "A8", name: "Video"},
            {img: "A9", name: "Yorum Duvarı"}];
        $scope.status_task = [{id: "0", icon: "ion-ios-clock-outline", color: "#727272"}
            , {id: "1", title: "", icon: "ion-ios-clock-outline", color: "#ea654f"},
            {id: "2", icon: "ion-ios-checkmark", color: "#98aa35"}];
        $scope.programsGo = function () {
            console.log("my name is");
            $state.go("app.programs")
        };

        $scope.show1 = function (title) {
            var hideSheet = $ionicActionSheet.show({
                buttons: [
                    {text: '<i class="icon positive ion-grid"></i><b class="positive">Function1</b>'},
                    {text: '<i class="icon assertive ion-navicon-round"></i><b class="assertive">Function2</b>'}
                ],
                /*destructiveText: 'Delete',*/
                titleText: "<h2>" + title + "</h2>",
                /*cancelText: 'Cancel',*/
                cancel: function () {
                    console.log('cancel clicked');
                },
                destructiveButtonClicked: function () {
                    console.log('DESTRUCT');
                    return true;
                },
                buttonClicked: function (index) {
                    console.log(index);
                    return true;
                }
            });

            // For example's sake, hide the sheet after two seconds
            /*$timeout(function() {
             hideSheet();
             }, 2000);*/

        };


        $scope.playlists = [
            {title: 'Reggae', id: 1},
            {title: 'Chill', id: 2},
            {title: 'Dubstep', id: 3},
            {title: 'Indie', id: 4},
            {title: 'Rap', id: 5},
            {title: 'Cowbell', id: 6}
        ];
    })

    .controller('listLargeImgCtrl', function ($scope, $stateParams) {
    });
